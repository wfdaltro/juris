import { Component, OnInit, AfterContentInit, Input, ContentChild } from '@angular/core';
import { FormControlName } from '../../../../node_modules/@angular/forms';

@Component({
  selector: 'app-input-container',
  templateUrl: './input-container.component.html',
  styleUrls: ['./input-container.component.css']
})
export class InputContainerComponent implements OnInit, AfterContentInit {

  @Input() label: string;

  @Input() errorMessage: string;

  @ContentChild(FormControlName) control: FormControlName;

  constructor() { }

  ngOnInit() {
  }

  ngAfterContentInit() {
    if(this.control === undefined) {
      throw new Error('Esse componente precisa ser usado com uma diretiva formControlName');
    }
  }

  hasSuccess(): boolean {
    return this.control.valid && (this.control.dirty || this.control.touched);
  }

  hasError(): boolean {
    return this.control.invalid && (this.control.dirty || this.control.touched);
  }

}
