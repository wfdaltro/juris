import { Error500Component } from './error-500/error-500.component';
import { Error404Component } from './error-404/error-404.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    Error404Component,
    Error500Component
  ],
  exports: [
    Error404Component,
    Error500Component
  ]
})
export class ErrorModule { }
