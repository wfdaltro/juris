import { DashboardComponent } from './feature/dashboard/dashboard.component';
import { LayoutComponent } from './layouts/layout.component';
import { Error404Component } from './error/error-404/error-404.component';
import { Error500Component } from './error/error-500/error-500.component';
import { LockscreenComponent } from './auth/lockscreen/lockscreen.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { LoginComponent } from './auth/login/login.component';
import { Routes } from '@angular/router';

export const ROUTES: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {
      'path': '',
      'component': LayoutComponent,
      'children': [
          {
              path: 'dashboard',
              component: DashboardComponent
          }
      ]
  },
  {
      'path': 'login',
      'component': LoginComponent
  },
  {
      'path': 'lockscreen',
      'component': LockscreenComponent
  },
  {
      'path': 'forgot_password',
      'component': ForgotPasswordComponent
  },
  {
      'path': 'error_404',
      'component': Error404Component
  },
  {
      'path': 'error_500',
      'component': Error500Component
  },
  {
      'path': '**',
      'redirectTo': 'error_404',
      'pathMatch': 'full'
  },
];
