import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from './login.service';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit,  OnDestroy {

  private loginForm: FormGroup;

  constructor(private loginService: LoginService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    $('body').addClass('empty-layout bg-silver-300');
    this.createLoginForm();
  }

  ngOnDestroy() {
    $('body').removeClass('empty-layout bg-silver-300');
  }

  public createLoginForm(): any {
    this.loginForm = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(8)]],
      user: ['', Validators.required]
    });
  }

  public autenticar(): void {

    this.loginService.autenticar(this.loginForm.value.usuario, this.loginForm.value.senha).subscribe(usuario =>{
      this.router.navigate(['/dashboard']);
    });
  }

}
