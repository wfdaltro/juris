import { RouterModule } from '@angular/router';
import { SharedModule } from './../shared/shared.module';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LockscreenComponent } from './lockscreen/lockscreen.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';

import {PasswordModule} from 'primeng/password';
import {InputTextModule} from 'primeng/inputtext';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    PasswordModule,
    InputTextModule
  ],
  declarations: [
    LoginComponent,
    LockscreenComponent,
    ForgotPasswordComponent,
  ],
  exports: [
    LoginComponent,
    LockscreenComponent,
    ForgotPasswordComponent
  ],
  providers: [
    LoginService
  ]
})
export class AuthModule { }
