package br.net.cactus.juris.dominio.base.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonDeserialize(using = EnumModelDeserializer.class)
public interface EnumModel<K> {

	K getCodigo();

	String getDescricao();

	default String getType() {
		return this.getClass().getSimpleName();
	}

	default boolean matchCodigo(K codigo) {
		return getCodigo().equals(codigo);
	}

}
