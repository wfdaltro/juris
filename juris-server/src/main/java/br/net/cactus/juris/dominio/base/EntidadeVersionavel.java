package br.net.cactus.juris.dominio.base;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

@MappedSuperclass
public abstract class EntidadeVersionavel<PK> extends Entidade<PK> {

	@Version
	@Column(name = "NU_VERSAO")
	private Long versao;

	public Long getVersao() {
		return versao;
	}

	public void setVersao(Long versao) {
		this.versao = versao;
	}

}
