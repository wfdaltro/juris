package br.net.cactus.juris.dominio.base;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;

public class FiltroPesquisa extends LinkedHashMap<String, String> {

	private static final String DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";

	private static final String DATE_FORMAT = "dd/MM/yyyy";

	public FiltroPesquisa(Map<String, String> values) {
		this.putAll(values);
	}

	public boolean hasParam(String key) {
		return containsKey(key) && get(key) != null;
	}

	public String getStringParam(String key) {
		return hasParam(key) ? get(key).toString() : null;
	}

	public Integer getIntParam(String key) {
		return hasParam(key) ? Integer.parseInt(getStringParam(key)) : null;
	}

	public Long getLongParam(String key) {
		return hasParam(key) ? Long.parseLong(getStringParam(key)) : null;
	}

	public Boolean getBooleanParam(String key) {
		return hasParam(key) ? Boolean.parseBoolean(getStringParam(key)) : null;
	}

	public Double getDoubleParam(String key) {
		return hasParam(key) ? Double.parseDouble(getStringParam(key)) : null;
	}

	public LocalDate getLocalDateParam(String key) {
		return hasParam(key) ? parseLocalDate(key) : null;
	}

	public LocalDateTime getLocalDateTimeParam(String key) {
		return hasParam(key) ? parseLocalDateTime(key) : null;
	}

	private LocalDate parseLocalDate(String key) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
		return LocalDate.parse(key, formatter);
	}

	private LocalDateTime parseLocalDateTime(String key) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
		return LocalDateTime.parse(key, formatter);
	}

}
