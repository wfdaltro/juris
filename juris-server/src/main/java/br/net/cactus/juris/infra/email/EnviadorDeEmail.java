package br.net.cactus.juris.infra.email;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class EnviadorDeEmail {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnviadorDeEmail.class);

	protected JavaMailSender mailSender;

	@Value("${spring.mail.from}")
	private String emailRemetente;

	@Autowired
	public EnviadorDeEmail(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public EmailStatus enviarEmailSimples(String to, String subject, String message) {
		return enviar(to, subject, message, false);
	}

	public EmailStatus enviarEmailHtml(String to, String subject, String htmlBody) {
		return enviar(to, subject, htmlBody, true);
	}

	private EmailStatus enviar(String to, String subject, String text, Boolean isHtml) {
		try {
			MimeMessage mail = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mail, true);
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText(text, isHtml);
			helper.setFrom(emailRemetente);
			mailSender.send(mail);
			LOGGER.info("Email enviado de: '{}' para: {}", subject, to);
			return new EmailStatus(to, subject, text).success();
		} catch (Exception e) {
			LOGGER.error(String.format("Não foi possível enviar email para: {}. Ocorreu o seguinte erro: {}", to,
					e.getMessage()));
			return new EmailStatus(to, subject, text).error(e.getMessage());
		}
	}

}
