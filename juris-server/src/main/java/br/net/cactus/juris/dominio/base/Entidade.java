package br.net.cactus.juris.dominio.base;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@MappedSuperclass
public abstract class Entidade<PK> implements Serializable {

	private static final int VALOR_INICIAL_GERACAO_HASHCODE = 7;
	
	private static final int MULTIPLICADOR_HASHCODDE = 37;
	
	private static final boolean USAR_CAMPOS_TRANSIENTS = Boolean.FALSE;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private PK pk;

	public PK getPk() {
		return pk;
	}

	public void setPk(PK pk) {
		this.pk = pk;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(VALOR_INICIAL_GERACAO_HASHCODE, MULTIPLICADOR_HASHCODDE, this,
				USAR_CAMPOS_TRANSIENTS);
	}

	@Override
	public boolean equals(Object entidadeComparada) {
		return EqualsBuilder.reflectionEquals(this, entidadeComparada, USAR_CAMPOS_TRANSIENTS,
				this.getClass().getSuperclass(), getEqualsExcludedFields());
	}

	protected String[] getEqualsExcludedFields() {
		return null;
	}

}
