package br.net.cactus.juris.dominio.administracao.empresa;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import br.net.cactus.juris.dominio.base.EntidadeVersionavel;

@Entity
@Table(name = "TB_EMPRESA")
@AttributeOverride(name = "pk", column = @Column(name = "ID_EMPRESA", columnDefinition = "serial"))
public class Empresa extends EntidadeVersionavel<Long> {

	@Column(name = "nm_empresa", length = 50)
	private String razaoSocial;

	private String nomeFantasia;

	private String cnpj;

	@Embedded
	private Endereco endereco;

	@Lob
	private byte[] logo;

}
