package br.net.cactus.juris.dominio.geral.cidade;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.net.cactus.juris.dominio.base.Entidade;
import br.net.cactus.juris.dominio.geral.estado.Estado;


@Entity
@Table(name = "TB_CIDADE")
@AttributeOverride(name = "pk", column = @Column(name = "ID_CIDADE"))
public class Cidade extends Entidade<Long> {

	@Column(name = "DS_NOME", length = 200, nullable = false)
	private String nome;

	@ManyToOne(optional = false)
	@JoinColumn(name = "ID_ESTADO")
	private Estado estado;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

}
