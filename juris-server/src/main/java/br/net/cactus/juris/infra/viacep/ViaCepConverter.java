package br.net.cactus.juris.infra.viacep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.net.cactus.juris.dominio.administracao.empresa.Endereco;
import br.net.cactus.juris.dominio.geral.cidade.CidadeRepository;

@Component
public class ViaCepConverter {

	@Autowired
	private CidadeRepository cidadeRepository;

	public ViaCepConverter(CidadeRepository cidadeRepository) {
		this.cidadeRepository = cidadeRepository;
	}

	public Endereco toEndereco(EnderecoVo vo) {
		Endereco endereco = new Endereco();
		endereco.setBairro(vo.getBairro());
		endereco.setCep(vo.getCep());
		endereco.setCidade(cidadeRepository.findCidadeNoEstado(vo.getUf(), vo.getLocalidade()));
		endereco.setComplementoLogradouro(vo.getComplemento());
		endereco.setLogradouro(vo.getLogradouro());
		return endereco;
	}

}
