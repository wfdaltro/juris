package br.net.cactus.juris.dominio.base;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public interface IBaseService<T extends Entidade<PK>, PK extends Serializable> {

	Collection<T> listar(Map<String, String> params);

	T obter(PK id);

	T salvar(T entidade);

	void excluir(PK id);

}
