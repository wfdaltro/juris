package br.net.cactus.juris.infra.viacep;

public class EnderecoNaoEncontradoException extends RuntimeException {

	public EnderecoNaoEncontradoException() {
		super("Endereco não encontrado.");
	}

}
