package br.net.cactus.juris;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {


	@Override
	public void onApplicationEvent(final ApplicationReadyEvent event) {
		this.loadRegioes();
		this.loadEstados();
		this.loadCidades();
	}

	private void loadRegioes() {
		
		
	}

	private void loadEstados() {
		// TODO Auto-generated method stub
		
	}

	private void loadCidades() {
		// TODO Auto-generated method stub
		
	}

}