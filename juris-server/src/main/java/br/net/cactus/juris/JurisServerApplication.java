package br.net.cactus.juris;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class JurisServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(JurisServerApplication.class, args);
	}

	@Bean
	@Autowired
	public CommandLineRunner startCommandLineRunner(final ApplicationContext applicationContext) {
		return args -> {
			displayAllBeans(applicationContext);
		};
	}

	private void displayAllBeans(final ApplicationContext applicationContext) {
		System.out.println("Application.displayAllBeans:");
		final String[] allBeanNames = applicationContext.getBeanDefinitionNames();
		for (final String beanName : allBeanNames) {
			System.out.println(beanName);
		}
	}

}
