package br.net.cactus.juris.dominio.base.enums;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

@SuppressWarnings("rawtypes")
public class EnumModelDeserializer extends JsonDeserializer<EnumModel> {

	private final String ENUM_PACKAGE = "br.gov.bcb.tributos.negocio.enumeracoes.";

	@Override
	public EnumModel deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		Class clazz = (Class) ctxt.getContextualType().getRawClass();
		System.out.println(clazz);
		JsonNode node = jp.getCodec().readTree(jp);
		return findEnum(node.get("type").asText(), node.get("codigo").asText());
	}

	private EnumModel findEnum(String type, String codigo) {
		try {
			Class clazz = Class.forName(ENUM_PACKAGE + type);
			return EnumModelHelper.obterInstancia(clazz.getEnumConstants(), codigo);
		} catch (ClassNotFoundException e) {
			return null;
		}
	}

}
