package br.net.cactus.juris.seguranca;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.net.cactus.juris.dominio.administracao.usuario.Usuario;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	JwtAuthenticationRepository repository;

	@Autowired
	public JwtUserDetailsService(JwtAuthenticationRepository repository) {
		this.repository = repository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario dadosUsuario = repository.loadUserByUsername(username);
		return new JwtUserDetails(dadosUsuario);

	}

}
