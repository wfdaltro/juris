package br.net.cactus.juris.seguranca;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.net.cactus.juris.dominio.administracao.perfil.Perfil;
import br.net.cactus.juris.dominio.administracao.usuario.SituacaoUsuario;
import br.net.cactus.juris.dominio.administracao.usuario.Usuario;

@Repository
@PropertySource("classpath:queries/queries.xml")
public class JwtAuthenticationRepository {

	private JdbcTemplate jdbcTemplate;

	@Value("${usuario.porLogin}")
	private String queryUsuarioPorLogin;

	@Value("${perfil.porIdUsuario}")
	private String queryPerfilPorIdUsuario;

	@Autowired
	public JwtAuthenticationRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Transactional(readOnly = true)
	public Usuario loadUserByUsername(String username) {
		try {
			Usuario usuario = jdbcTemplate.queryForObject(queryUsuarioPorLogin, new Object[] { username },
					new UsuarioRowMapper());
			List<Perfil> perfis = jdbcTemplate.query(queryPerfilPorIdUsuario, new Object[] { usuario.getPk() },
					new PerfilRowMapper());
			usuario.setPerfis(new HashSet<>(perfis));
			return usuario;
		} catch (EmptyResultDataAccessException e) {
			throw new UsernameNotFoundException(username);
		}
	}

	private class UsuarioRowMapper implements RowMapper<Usuario> {
		public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
			Usuario usuario = new Usuario();
			usuario.setPk(rs.getLong("ID_USUARIO"));
			usuario.setNome(rs.getString("NM_USUARIO"));
			usuario.setLogin(rs.getString("DS_LOGIN"));
			usuario.setSituacao(SituacaoUsuario.get(rs.getInt("CD_SITUACAO")));
			usuario.setSenha(rs.getString("DS_SENHA"));
			return usuario;
		}

	}

	public class PerfilRowMapper implements RowMapper<Perfil> {
		public Perfil mapRow(ResultSet rs, int rowNum) throws SQLException {
			Perfil perfil = new Perfil();
			perfil.setPk(rs.getShort("ID_PERFIL"));
			perfil.setNome(rs.getString("DS_NOME"));
			perfil.setSigla(rs.getString("DS_SIGLA"));
			return perfil;
		}

	}

}
