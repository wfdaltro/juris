package br.net.cactus.juris.dominio.geral.cidade;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CidadeRepository extends JpaRepository<Cidade, Long> {

	@Query("select c from Cidade c where c.estado.sigla = :uf and c.nome = :localidade")
	Cidade findCidadeNoEstado(String uf, String cidade);

}
