package br.net.cactus.juris.infra.viacep;

import java.text.MessageFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import br.net.cactus.juris.dominio.administracao.empresa.Endereco;

@Repository
public class ViaCepRepository {

	private RestTemplate restTemplate;
	
	private ViaCepConverter viaCepConverter;

	@Autowired
	public ViaCepRepository(RestTemplate restTemplate, ViaCepConverter viaCepConverter) {
		this.restTemplate = restTemplate;
		this.viaCepConverter = viaCepConverter;
	}

	public Endereco findEnderecoByCep(String cep) {
		String URI = MessageFormat.format("https://viacep.com.br/ws/{0}/json/", cep);
		ResponseEntity<EnderecoVo> response = this.restTemplate.getForEntity(URI, EnderecoVo.class);
		if (HttpStatus.OK.equals(response.getStatusCode())) {
			return viaCepConverter.toEndereco(response.getBody());
		}
		throw new EnderecoNaoEncontradoException();
	}

}
