package br.net.cactus.juris.seguranca;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import br.net.cactus.juris.dominio.administracao.perfil.Perfil;
import br.net.cactus.juris.dominio.administracao.usuario.SituacaoUsuario;
import br.net.cactus.juris.dominio.administracao.usuario.Usuario;

class JwtUserDetails implements UserDetails {

	private Long id;

	private String login;

	private String senha;

	private boolean usuarioCancelado;

	private boolean usuarioBloqueado;

	private boolean senhaExpirada;

	private Collection<? extends GrantedAuthority> authorities;

	public JwtUserDetails(Usuario dadosUsuario) {
		super();
		this.id = dadosUsuario.getPk();
		this.login = dadosUsuario.getLogin();
		this.senha = dadosUsuario.getSenha();
		SituacaoUsuario situacao =  dadosUsuario.getSituacao();
		this.usuarioCancelado = SituacaoUsuario.CANCELADO == situacao;
		this.usuarioBloqueado = SituacaoUsuario.BLOQUEADO == situacao;
		this.senhaExpirada = false;
		this.authorities = dadosUsuario.getPerfis().stream().map(perfil -> new SimpleGrantedAuthority(perfil.getSigla())).collect(Collectors.toList());

	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public String getPassword() {
		return senha;
	}

	@Override
	public String getUsername() {
		return login;
	}

	@Override
	public boolean isAccountNonExpired() {
		return !this.senhaExpirada;
	}

	@Override
	public boolean isAccountNonLocked() {
		return !this.usuarioBloqueado;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return !this.senhaExpirada;
	}

	@Override
	public boolean isEnabled() {
		return !this.usuarioCancelado;
	}

	public boolean hasRole(Perfil perfil) {
		return getAuthorities().contains(new SimpleGrantedAuthority(perfil.getSigla()));
	}

}
