package br.net.cactus.juris.dominio.administracao.empresa;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.net.cactus.juris.dominio.geral.cidade.Cidade;

@Embeddable
public class Endereco {

	@Column(name = "ds_logradouro", length = 50)
	private String logradouro;

	@Column(name = "ds_numero_logradouro", length = 5)
	private String numeroLogradouro;

	@Column(name = "ds_complemento_logradouro", length = 20)
	private String complementoLogradouro;

	@Column(name = "ds_bairro", length = 50)
	private String bairro;

	@Column(name = "ds_cep", length = 8)
	private String cep;

	@ManyToOne(optional = false)
	@JoinColumn(name = "ID_CIDADE")
	private Cidade cidade;

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumeroLogradouro() {
		return numeroLogradouro;
	}

	public void setNumeroLogradouro(String numeroLogradouro) {
		this.numeroLogradouro = numeroLogradouro;
	}

	public String getComplementoLogradouro() {
		return complementoLogradouro;
	}

	public void setComplementoLogradouro(String complementoLogradouro) {
		this.complementoLogradouro = complementoLogradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

}
