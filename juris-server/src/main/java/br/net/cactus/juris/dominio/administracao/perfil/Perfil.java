package br.net.cactus.juris.dominio.administracao.perfil;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import br.net.cactus.juris.dominio.administracao.usuario.Usuario;
import br.net.cactus.juris.dominio.base.Entidade;

@Entity
@Table(name = "TB_PERFIL")
@AttributeOverride(name = "pk", column = @Column(name = "ID_PERFIL", columnDefinition = "serial"))
public class Perfil extends Entidade<Short> {

	@Column(name = "DS_SIGLA", nullable = false, unique = true, length = 15)
	private String sigla;

	@Column(name = "DS_NOME", nullable = false, unique = true, length = 60)
	private String nome;
	
	@ManyToMany(mappedBy = "perfis", fetch=FetchType.LAZY)  
	private Set<Usuario> usuarios;

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
