package br.net.cactus.juris.dominio.tarefa;

import java.time.LocalDateTime;

import br.net.cactus.juris.dominio.base.EntidadeAuditavel;

public class Tarefa  {
	
	private String descricao;
	
	private PrioridadeTarefa prioridade;
	
	private LocalDateTime dataHoraInicio;
	
	private LocalDateTime dataHoraFim;

}
