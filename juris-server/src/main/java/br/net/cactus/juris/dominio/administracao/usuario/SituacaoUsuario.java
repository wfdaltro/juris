package br.net.cactus.juris.dominio.administracao.usuario;

import br.net.cactus.juris.dominio.base.enums.EnumModel;
import br.net.cactus.juris.dominio.base.enums.EnumModelConverter;
import br.net.cactus.juris.dominio.base.enums.EnumModelHelper;

public enum SituacaoUsuario implements EnumModel<Integer> {

	ATIVO(1, "Ativo"), BLOQUEADO(2, "Bloqueado"), CANCELADO(3, "Cancelado");

	private Integer codigo;

	private String descricao;

	private SituacaoUsuario(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public static class Converter extends EnumModelConverter<SituacaoUsuario, Integer> {
		public Converter() {
			super(SituacaoUsuario.class);
		}
	}

	public static SituacaoUsuario get(int codigo) {
		return EnumModelHelper.obterInstancia(SituacaoUsuario.values(), codigo);
	}

}
