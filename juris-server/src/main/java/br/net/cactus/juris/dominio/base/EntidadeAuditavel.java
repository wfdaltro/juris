package br.net.cactus.juris.dominio.base;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@EntityListeners(EntidadeAuditavelListener.class)
public abstract class EntidadeAuditavel<PK> extends EntidadeVersionavel<PK> {

	@Column(name = "ds_login_usuario_inclusao", length = 20)
	private String usuarioInclusao;

	@Column(name = "ds_login_usuario_alteracao", length = 20)
	private String usuarioAlteracao;

	@Column(name = "dh_inclusao")
	private LocalDateTime dataHoraInclusao;

	@Column(name = "dh_alteracao")
	private LocalDateTime dataHoraAlteracao;

	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	public String getUsuarioAlteracao() {
		return usuarioAlteracao;
	}

	public void setUsuarioAlteracao(String usuarioAlteracao) {
		this.usuarioAlteracao = usuarioAlteracao;
	}

	public LocalDateTime getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	public void setDataHoraInclusao(LocalDateTime dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	public LocalDateTime getDataHoraAlteracao() {
		return dataHoraAlteracao;
	}

	public void setDataHoraAlteracao(LocalDateTime dataHoraAlteracao) {
		this.dataHoraAlteracao = dataHoraAlteracao;
	}

}