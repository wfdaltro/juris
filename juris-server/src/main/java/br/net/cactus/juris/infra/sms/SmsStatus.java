package br.net.cactus.juris.infra.sms;

import java.util.Arrays;

public class SmsStatus {

	public static final String SUCCESS = "SUCCESS";

	public static final String ERROR = "ERROR";

	private String destinatario;

	private StatusSms statusRetorno;

	public SmsStatus(String apiResponse, String destinatario) {
		this.destinatario = destinatario;
		String[] valores = apiResponse.split(",");
		this.statusRetorno = StatusSms.get(valores[1]);
	}

	public String getDestinatario() {
		return destinatario;
	}

	public String getDescricao() {
		return statusRetorno.getDescricao();
	}

	public String getCodigo() {
		return statusRetorno.getCodigo();
	}

	public String getStatus() {
		return statusRetorno.getStatus();
	}

	private enum StatusSms {

		LOGIN_INVALIDO("1", "Login inválido.", ERROR),
		USUARIO_SEM_CREDITO("2", "Usuário sem créditos para enviar mensagens", ERROR),
		CELULAR_INVALIDO("3", "Celular do destinatário é inválido.", ERROR),
		MENSAGEM_INVALIDA("4", "A mensagem é inválida para o envio.", ERROR),
		MENSAGEM_AGENDADA("5", "A mensagem foi agendada para envio.", ERROR),
		MENSAGEM_ENVIADA("6", "A mensagem foi encaminhada para a operadora.", SUCCESS);

		private String codigo;

		private String descricao;

		private String status;

		private StatusSms(String codigo, String descricao, String status) {
			this.codigo = codigo;
			this.descricao = descricao;
			this.status = status;
		}

		public static StatusSms get(String codigo) {
			return Arrays.stream(StatusSms.values()).filter(e -> e.codigo.equals(codigo)).findFirst()
					.orElseThrow(() -> new IllegalStateException(String.format("Constante inexistente %s.", codigo)));
		}

		public String getDescricao() {
			return descricao;
		}

		public String getCodigo() {
			return codigo;
		}

		public String getStatus() {
			return status;
		}

	}

}
