package br.net.cactus.juris.dominio.administracao.usuario;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import br.net.cactus.juris.dominio.administracao.perfil.Perfil;
import br.net.cactus.juris.dominio.base.EntidadeVersionavel;

@Entity
@Table(name = "TB_USUARIO")
@AttributeOverride(name = "pk", column = @Column(name = "ID_USUARIO", columnDefinition = "serial"))
public class Usuario extends EntidadeVersionavel<Long> {

	@Column(name = "nm_usuario", nullable = false)
	private String nome;

	@Column(name = "in_trocar_senha")
	private Boolean inTrocarSenha;

	@Column(name = "ds_login", nullable = false, unique = true)
	private String login;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "im_usuario")
	private byte[] foto;

	@Column(name = "ds_senha", nullable = false)
	private String senha;

	@Column(name = "cd_situacao", nullable = false)
	private SituacaoUsuario situacao;

	@ManyToMany
	@JoinTable(name = "TR_USUARIO_PERFIL", joinColumns = { @JoinColumn(name = "ID_USUARIO") }, inverseJoinColumns = {
			@JoinColumn(name = "ID_PERFIL") })
	private Set<Perfil> perfis = new HashSet<Perfil>();

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getInTrocarSenha() {
		return inTrocarSenha;
	}

	public void setInTrocarSenha(Boolean inTrocarSenha) {
		this.inTrocarSenha = inTrocarSenha;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public SituacaoUsuario getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoUsuario situacao) {
		this.situacao = situacao;
	}

	public Set<Perfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(Set<Perfil> perfis) {
		this.perfis = perfis;
	}

}
