package br.net.cactus.juris.dominio.base;

import java.time.LocalDateTime;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import br.net.cactus.juris.seguranca.JwtUsuarioAutenticado;

public class EntidadeAuditavelListener {

	@PrePersist
	public void prePersist(EntidadeAuditavel target) {
		LocalDateTime now = LocalDateTime.now();
		target.setDataHoraInclusao(now);
		target.setUsuarioInclusao(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
	}

	@PreUpdate
	public void preUpdate(EntidadeAuditavel target) {
		LocalDateTime now = LocalDateTime.now();
		target.setDataHoraAlteracao(now);
		target.setUsuarioAlteracao(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
	}

}
