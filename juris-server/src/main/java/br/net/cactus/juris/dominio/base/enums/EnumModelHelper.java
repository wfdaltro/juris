package br.net.cactus.juris.dominio.base.enums;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

public class EnumModelHelper {

	public static <T extends EnumModel<S>, S> T obterInstancia(T[] values, S codigo) {
		Stream<T> stream = Arrays.stream(values);
		try {
			return stream.filter(registro -> registro.matchCodigo(codigo)).findFirst().get();
		} catch (NoSuchElementException nses) {
			return null;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static EnumModel obterInstancia(Object[] enumConstants, String codigo) {
		Stream<EnumModel> stream = Arrays.stream((EnumModel[]) enumConstants);
		try {
			return stream.filter(registro -> registro.matchCodigo(codigo)).findFirst().get();
		} catch (NoSuchElementException nses) {
			return null;
		}
	}

}