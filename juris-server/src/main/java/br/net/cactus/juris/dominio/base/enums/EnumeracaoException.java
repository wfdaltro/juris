package br.net.cactus.juris.dominio.base.enums;

public class EnumeracaoException extends RuntimeException {

	public EnumeracaoException(String msg, Exception e) {
		super(msg, e);
	}

}
