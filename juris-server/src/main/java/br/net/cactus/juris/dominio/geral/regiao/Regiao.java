package br.net.cactus.juris.dominio.geral.regiao;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.net.cactus.juris.dominio.base.Entidade;

@Entity
@Table(name = "TB_REGIAO")
@AttributeOverride(name = "pk", column = @Column(name = "ID_REGIAO"))
public class Regiao extends Entidade<Integer> {

	@Column(name = "DS_SIGLA", length = 2, nullable = false)
	private String sigla;

	@Column(name = "DS_NOME", length = 50, nullable = false)
	private String nome;

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
