package br.net.cactus.juris.infra.sms;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class EnviadorDeSms {

	private RestTemplate restTemplate;

	@Value("${sms.api.url}")
	private String smsApiUrl;

	@Value("${sms.api.usuario}")
	private String smsApiUsuario;

	@Value("${sms.api.senha}")
	private String smsApiSenha;

	@Autowired
	public EnviadorDeSms(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public void enviarSms(String destinatario, String mensagem) {
		try {
			String URI = smsApiUrl
					.concat("?user=").concat(smsApiUsuario)
					.concat("&password=").concat(smsApiSenha)
					.concat("&destinatario=").concat(destinatario)
					.concat("&msg=").concat(URLEncoder.encode(mensagem, "UTF-8"));
			String result = restTemplate.getForObject(URI, String.class);
			System.out.println(result);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException("A mensagem não pode ser codificada para o envio.");
		}

	}

}
