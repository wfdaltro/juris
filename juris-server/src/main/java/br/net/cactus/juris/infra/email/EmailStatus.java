package br.net.cactus.juris.infra.email;
public class EmailStatus {
 
    public static final String SUCCESS = "SUCCESS";
    
    public static final String ERROR = "ERROR";
 
    private final String to;
    
    private final String subject;
    
    private final String from;
 
    private String status;
    
    private String errorMessage;
 
    public EmailStatus(String to, String subject, String from) {
        this.to = to;
        this.subject = subject;
        this.from = from;
    }
 
    public EmailStatus success() {
        this.status = SUCCESS;
        return this;
    }
 
    public EmailStatus error(String errorMessage) {
        this.status = ERROR;
        this.errorMessage = errorMessage;
        return this;
    }
 
    public boolean isSuccess() {
        return SUCCESS.equals(this.status);
    }
 
    public boolean isError() {
        return ERROR.equals(this.status);
    }
 
    public String getTo() {
        return to;
    }
 
    public String getSubject() {
        return subject;
    }
 
    public String getFrom() {
        return from;
    }
 
    public String getStatus() {
        return status;
    }
 
    public String getErrorMessage() {
        return errorMessage;
    }
}