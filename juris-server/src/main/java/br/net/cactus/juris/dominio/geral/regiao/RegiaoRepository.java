package br.net.cactus.juris.dominio.geral.regiao;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RegiaoRepository extends JpaRepository<Regiao, Integer> {

}
