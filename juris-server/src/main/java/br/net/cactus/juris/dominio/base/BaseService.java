package br.net.cactus.juris.dominio.base;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract class BaseService<T extends Entidade<PK>, PK extends Serializable> implements IBaseService<T, PK> {

	private IBaseRepository<T, PK> repository;

	@Autowired
	public BaseService(IBaseRepository<T, PK> repository) {
		this.repository = repository;
	}

	@Override
	public Collection<T> listar(Map<String, String> params) {
		return null;
	}

	@Override
	public T obter(PK id) {
		return null;
	}

	@Override
	public T salvar(T entidade) {
		return null;
	}

	@Override
	public void excluir(PK id) {

	}

	public JpaRepository<T, PK> getRepository() {
		return repository;
	}

}
