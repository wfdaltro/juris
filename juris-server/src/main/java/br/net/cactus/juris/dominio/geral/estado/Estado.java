package br.net.cactus.juris.dominio.geral.estado;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.net.cactus.juris.dominio.base.Entidade;
import br.net.cactus.juris.dominio.geral.regiao.Regiao;

@Entity
@Table(name = "TB_ESTADO")
@AttributeOverride(name = "pk", column = @Column(name = "ID_ESTADO"))
public class Estado extends Entidade<Integer> {

	@Column(name = "DS_SIGLA", length = 2, nullable = false)
	private String sigla;

	@Column(name = "DS_NOME", length = 100, nullable = false)
	private String nome;

	@ManyToOne(optional = false)
	@JoinColumn(name = "ID_REGIAO")
	private Regiao regiao;

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Regiao getRegiao() {
		return regiao;
	}

	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}

}
